package com.peixoto.loja.use;

import com.peixoto.loja.exceptions.DomainException;

public class ValidaLetras {
	
    public static boolean validaLetras(String nome) {
    	boolean valido = true;
    	if(!nome.matches("[a-zA-Z]")) {
    		throw new DomainException();
        }
    	return valido;
    }
}
