package com.peixoto.loja.service;

import com.peixoto.loja.domain.Produto;
import com.peixoto.loja.exceptions.EstoqueException;


public class EstoqueService {

    public void decrementarEstoque(Produto produto, Integer quantidade) {
        produto.setEstoque(produto.getEstoque() - quantidade);
     }
     
     public void incrementarEstoque(Produto produto, Integer quantidade) {
    	 produto.setEstoque(produto.getEstoque() + quantidade);
      }

     
    public void validaString(String string) throws EstoqueException {
    	    	 
    	 if(string.isEmpty()) {
    		 throw new EstoqueException("Campo inv�lido - campo n�o deve ser vazio");
    	 } 
    	 
    	 if(string == "0") {
    		 throw new EstoqueException("Campo inv�lido - campo n�o deve ser zero");
    	 } 
   }
    
    public void validaDouble(Double valor) throws EstoqueException {
   	 
   	 if( valor == 0) {
   		 throw new EstoqueException("Campo inv�lido - campo n�o deve ser vazio ou zero");
   	 }  	 
  }
    
    
}
