package com.peixoto.loja.tela;

import com.peixoto.loja.domain.Produto;
import com.peixoto.loja.exceptions.EstoqueException;
import com.peixoto.loja.repositorio.ProdutoRepositorio;
import com.peixoto.loja.service.EstoqueService;

import java.util.List;
import java.util.Scanner;

public class TelaEstoque implements Tela {

    final private Scanner entradaEstoque;
    private ProdutoRepositorio produtoRepositorio;
    private EstoqueService estoqueService;
    
    public TelaEstoque(Scanner entradaEstoque) {
        this.entradaEstoque = entradaEstoque;
        this.produtoRepositorio = new ProdutoRepositorio();
        this.estoqueService = new EstoqueService();
    }

    public void abrirTela() {
    
        System.out.println("Bem vindo ao sistema de Estoque amigo Adm");
        int opcao = 0;
        do {
            System.out.println("0 - Sair do sistema de estoque");
            System.out.println("1 - Cadastrar Produto");
            System.out.println("2 - Listar Produtos");
            System.out.println("3 - Estoque disponivel do Produto");
            opcao = Integer.parseInt(this.entradaEstoque.nextLine());
            
            if(opcao == 1) {
                entrarProdutoNaTela();
            }
            
            if(opcao == 2) {
                List<Produto> produtos = this.produtoRepositorio.buscarTodos();
                mostrarProdutos(produtos);
            }
            if (opcao == 3) {
                System.out.println("Informe o codigo do produto: ");
                Integer codigoProdutoASerPesquisado = Integer.parseInt(this.entradaEstoque.nextLine());
                Produto produtoDoEncontradoNaBusca = this.produtoRepositorio.buscarProdutoPorCodigo(codigoProdutoASerPesquisado);
                System.out.println("O Estoque �: ");
                System.out.println(produtoDoEncontradoNaBusca.getEstoque());
            }
            
        } while (opcao != 0);
        
    }

    private void mostrarProdutos(List<Produto> produtos) {
        for(Produto produto : produtos) {
            System.out.println("Codigo: " + produto.getCodigo() +" | Nome: " + produto.getNome() + " | Valor: " + produto.getValor() + " | Quantidade estoque: " + produto.getEstoque() );
        }
    }

    private Produto entrarProdutoNaTela() {
        Produto produto = new Produto();
        
        try {                  	
        	System.out.println("Informe o Nome: ");
        	String nome = entradaEstoque.nextLine();
        	estoqueService.validaString(nome);
        	produto.setNome(nome);
        	
        	System.out.println("Informe a Descri��o: ");
        	String descricao = entradaEstoque.nextLine();
        	estoqueService.validaString(descricao);
        	produto.setDescricao(descricao);
        
        	System.out.println("Informe o Valor: ");
        	Double valor = Double.parseDouble(entradaEstoque.nextLine());
        	estoqueService.validaDouble(valor);
        
        	System.out.println("Informe o Estoque: ");
        	Integer estoque = Integer.parseInt(this.entradaEstoque.nextLine());
        
        	produto.setValor(valor);
        	produto.setEstoque(estoque);     	
        	produto.setCodigo(produtoRepositorio.criarCodigoProduto(produto));
        	
        	this.produtoRepositorio.salvar(produto);
        	System.out.println("Produto cadastrado com sucesso!");
        	System.out.println("");
        		
        }catch (EstoqueException e){
        	e.printStackTrace();
        	System.out.println(e.getMessage());
        		
        }catch(NumberFormatException e){
    		System.out.println("Erro: este campo deve conter um n�mero");
    		
    	}catch(RuntimeException e){
    		System.out.println("Erro inesperado: " + e.getMessage());
    	}
        
        return produto;
        
    }
    
}
