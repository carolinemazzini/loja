package com.peixoto.loja.tela;

import java.util.List;
import java.util.Scanner;

import com.peixoto.loja.domain.Cliente;
import com.peixoto.loja.domain.Endereco;
import com.peixoto.loja.repositorio.ClienteRepositorio;
import com.peixoto.loja.use.ValidaCPF;


public class TelaCliente implements Tela {

	final private Scanner entradaCliente;
    private ClienteRepositorio clienteRepositorio;
    
    public TelaCliente(Scanner entradaCliente) {
        this.entradaCliente = entradaCliente;
        this.clienteRepositorio = new ClienteRepositorio();
    }

    public void abrirTela() {
    	

        System.out.println("Bem vindo ao sistema de Cliente amigo Adm");
        int opcao = 0;
        do {
            System.out.println("0 - Sair do sistema de cliente");
            System.out.println("1 - Cadastrar Cliente");
            System.out.println("2 - Listar Cliente");
            System.out.println("3 - Consultar Cliente");
            opcao = Integer.parseInt(this.entradaCliente.nextLine());
            
            if(opcao == 1) {
                Cliente cliente = entrarClienteNaTela();
                this.clienteRepositorio.salvar(cliente);
            }
            
            if(opcao == 2) {
                List<Cliente> cliente = this.clienteRepositorio.buscarTodos();
                mostrarCliente(cliente);
            }
            
            if(opcao == 3) {
            	
            	int subOpcao = 0;
            	do {
            		System.out.println("Para consultar o cliente por CPF, digite 1");
            		System.out.println("Para consultar o cliente por nome, digite 2");
            		System.out.println("Para sair da consulta. digite 0");
            		subOpcao = Integer.parseInt(this.entradaCliente.nextLine());
            	
            			if(subOpcao == 1) {
            				System.out.println("Informe o CPF do cliente que deseja buscar: ");
            				String cpfPesquisado = this.entradaCliente.nextLine();
            				Cliente clienteEncontradoBusca = this.clienteRepositorio.buscarClientePorCPF(cpfPesquisado);
            				System.out.println("O Cliente �: ");
            				System.out.println(clienteEncontradoBusca.getNome());
            			
            			}if(subOpcao ==2) {
            				System.out.println("Informe o nome do cliente que deseja buscar: ");
            				String nomePesquisado = this.entradaCliente.nextLine();
            				List <Cliente> clienteEncontradoBusca = this.clienteRepositorio.buscarClientePorNome(nomePesquisado);
            				System.out.println("O Cliente �: ");
            					for(Cliente clienteNaLista : clienteEncontradoBusca) {
            						System.out.println(clienteNaLista.getNome());
            	      
            					}
            			}
            	
            	} while (subOpcao != 0);
            }
            
        } while (opcao != 0);

    }
    
    private void mostrarCliente(List<Cliente> listaCliente) {
    	
        for(Cliente cliente : listaCliente) {
            System.out.println("Nome: " + cliente.getNome()
                                + " | CPF: " + cliente.getCpf() 
                                + " | Endere�o: " + cliente.getEndereco().getLogradouro()
            		            + ", " + cliente.getEndereco().getNumero()
            		            + ", " + cliente.getEndereco().getCidade()
            		            + "| Data Reativa��o: " + cliente.verificarDataReativacao());
        	}
    }
    
   
    Cliente entrarClienteNaTela() {
        Cliente cliente = new Cliente();
        Endereco endereco = new Endereco();
       
        System.out.println("Informe o CPF: ");
        String CPF = entradaCliente.nextLine();
        
        while(ValidaCPF.isCPF(CPF) == false) {
            System.out.println("CPF inv�lido, tente novamente.");
            System.out.println("Informe o CPF: ");
            CPF = entradaCliente.nextLine();
        }
        
        cliente.setCpf(CPF);
        	
        System.out.println("Informe o Nome: ");
        String nome = this.entradaCliente.nextLine();
        	
        	while(nome.isEmpty()){
        		System.out.println("Nome inv�lido");
        		System.out.println("Digite um nome v�lido");
        		nome = this.entradaCliente.nextLine();
        	}
        	
        	cliente.setNome(nome.trim());
          
        System.out.println("Informe o logradouro/rua: ");
        endereco.setLogradouro(this.entradaCliente.nextLine());
        System.out.println("Informe o n�mero: ");
        endereco.setNumero(this.entradaCliente.nextLine());
        System.out.println("Informe o bairro: ");
        endereco.setBairro(this.entradaCliente.nextLine());
        System.out.println("Informe o cidade: ");
        endereco.setCidade(this.entradaCliente.nextLine());
        System.out.println("Informe o CEP: ");
        endereco.setCep(this.entradaCliente.nextLine());
        cliente.setEndereco(endereco);
        cliente.setDataReativacao(cliente.calcularDataReativacao());
        
        return cliente;
    }
}