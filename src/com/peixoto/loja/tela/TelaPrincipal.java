package com.peixoto.loja.tela;

import java.util.Scanner;

import com.peixoto.loja.exceptions.DomainException;

public class TelaPrincipal implements Tela{
	
	Tela tela;

    public void abrirTela() {
        int opcao = 0;
        Scanner entrada = new Scanner(System.in);
        System.out.println("***************Bem Vindo Amigo Administrador da Loja****************");
        do {
        	try {
        		System.out.println("0 - Fechar Sistema");
        		System.out.println("1 - Abrir Tela do Estoque");
        		System.out.println("2 - Abrir Tela de Cliente");
        		System.out.println("3 - Abrir Tela de Pedido");
        		opcao = Integer.parseInt(entrada.nextLine());
        	
        	
        		if(opcao == 1) {
        			tela = new TelaEstoque(entrada);
        			tela.abrirTela();
                
        		}if(opcao == 2) {
        			tela = new TelaCliente(entrada);
        			tela.abrirTela();
            
        		}if(opcao == 3) {
        			tela = new TelaPedido(entrada);
        			tela.abrirTela();
        		}
        	}
            catch(NumberFormatException e){
        		System.out.println("Op��o selecionada n�o existe");
        	}
        	catch(DomainException e){
        		System.out.println("Erro: " + e.getMessage());
        	}
        	catch(RuntimeException e){
        		System.out.println("Erro inesperado: " + e.getMessage());
        	}
            
        }
        while (opcao != 0);
        System.out.println("Fechou a Tela");
    }

}