package com.peixoto.loja.tela;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

import com.peixoto.loja.domain.Cliente;
import com.peixoto.loja.domain.Pedido;
import com.peixoto.loja.domain.ItemPedido;
import com.peixoto.loja.domain.Produto;
import com.peixoto.loja.exceptions.DomainException;
import com.peixoto.loja.repositorio.ClienteRepositorio;
import com.peixoto.loja.repositorio.ProdutoRepositorio;
import com.peixoto.loja.repositorio.PedidoRepositorio;
import com.peixoto.loja.service.EstoqueService;

public class TelaPedido implements Tela{
	
	final private Scanner entradaPedido;
    private ClienteRepositorio clienteRepositorio;
    private ProdutoRepositorio produtoRepositorio;
    private PedidoRepositorio pedidoRepositorio;
    private EstoqueService serviceEstoque;
    
    
    public TelaPedido(Scanner entradaPedido) {
        this.entradaPedido = entradaPedido;
        this.produtoRepositorio = new ProdutoRepositorio();
        this.clienteRepositorio = new ClienteRepositorio();
        this.pedidoRepositorio = new PedidoRepositorio();
        this.serviceEstoque = new EstoqueService();
    }
    
    
    public void abrirTela() {
    	   	
        int opcao = 0;
        int subOpcao = 0;
        
        do {
        	System.out.println("Bem vindo ao sistema de Pedido");
            System.out.println("1 - Se voc� possui um cadastro");
            System.out.println("2 - Se voc� ainda n�o possui um cadastro");
            System.out.println("0 - Se voc� deseja encerrar");
            opcao = Integer.parseInt(this.entradaPedido.nextLine());
            
            if(opcao == 1) {
            	
            	System.out.println("Informe o seu CPF : ");
                String cpfPesquisado = this.entradaPedido.nextLine();
            	Cliente clienteEncontradoBusca = this.clienteRepositorio.buscarClientePorCPF(cpfPesquisado);
            	
                subOpcao = mostrarMenuPedido(clienteEncontradoBusca);
                       
                
                if(subOpcao == 1) {

                	Pedido pedidoDesejado = new Pedido();
                	String retorno = "S";
                	String opcaoAltera = "S";
                	
                	mostraProdutosEstoque();
                	
                	realizarPedido(pedidoDesejado, retorno);
                	
                	System.out.println(pedidoDesejado.mostrarPedido());
                	
                	System.out.print("Deseja alterar a quantidade de algum item? S/N: ");
                	opcaoAltera = entradaPedido.nextLine();       	
        			alteraQtdItem(pedidoDesejado, opcaoAltera);
                	   	
            		this.pedidoRepositorio.salvar(pedidoDesejado);            		
                	System.out.println("");
                	System.out.println("Seu pedido foi finalizado.");
                	System.out.println(pedidoDesejado);
                	System.out.println("O total do seu pedido � de: R$" + pedidoDesejado.valorTotalPedido());
                	System.out.println("");

                }
            
                if(subOpcao == 2) {
                	
                	consultaPedido();                 
                }
            }
                           
            if(opcao == 2) {
            	System.out.println("Entre na tela de Cadastro para realizar  seu cadastro");
            }

        } while (opcao != 0);
        
    }


	private void mostraProdutosEstoque() {
		System.out.println("Veja abaixo os produtos que temos dispon�veis em estoque: ");
		List<Produto> produto = this.produtoRepositorio.buscarProdutosComEstoque();
		mostrarProdutoEstoque(produto);
		System.out.println("");
	}

	private void alteraQtdItem(Pedido pedidoDesejado, String opcaoAltera) {
		while(opcaoAltera.equalsIgnoreCase("S")){
			
			System.out.print("Informe o codigo do item que deseja alterar a quantidade: ");
			Integer codigoProduto = Integer.parseInt(entradaPedido.nextLine());
			
			for(ItemPedido itemBuscado : pedidoDesejado.getListaItemPedido()) {	
				if(itemBuscado.getProduto().getCodigo().equals(codigoProduto)) {
					serviceEstoque.incrementarEstoque(itemBuscado.getProduto(), itemBuscado.getQuantidade());
				}
				
				System.out.print("Informe a quantidade que deseja do produto: ");
				int quantidadeAlterada = Integer.parseInt(entradaPedido.nextLine());
				
				if(quantidadeAlterada == 0) {											
					pedidoDesejado.removeItemPedido(itemBuscado);
					break;
				}
				else {
					itemBuscado.setQuantidade(quantidadeAlterada);
					serviceEstoque.decrementarEstoque(itemBuscado.getProduto(), itemBuscado.getQuantidade());
					break;
				}				
			}
	
			System.out.print("Deseja alterar mais algum item? S/N: ");
			opcaoAltera = entradaPedido.nextLine();
		}
	}

	private int mostrarMenuPedido(Cliente clienteEncontradoBusca) {
		int subOpcao;
		
		if(clienteEncontradoBusca == null) {
			throw new DomainException("CPF n�o encontrado em nossa base de clientes");
		}
		
		System.out.println("Ol�, " + clienteEncontradoBusca.getNome() + ", o que voc� deseja fazer?");
		System.out.println();
		System.out.println("0 - Sair do sistema de Pedido");
		System.out.println("1 - Fazer um novo Pedido");
		System.out.println("2 - Consultar meus Pedidos");
		subOpcao = Integer.parseInt(this.entradaPedido.nextLine());
		return subOpcao;
	}

	private void consultaPedido() {
		int opcaoConsulta;
		System.out.println("1 - Para consultar seu pedido por c�digo");
		System.out.println("2 - Para consultar seu pedido por data");
		opcaoConsulta = Integer.parseInt(this.entradaPedido.nextLine());
		
		if(opcaoConsulta == 1) {
						
			System.out.println("Informe o c�digo do seu pedido");
			int codigoPedido = Integer.parseInt(this.entradaPedido.nextLine());
			System.out.println(pedidoRepositorio.buscarPedidoCodigo(codigoPedido));
			
			if(pedidoRepositorio.buscarPedidoCodigo(codigoPedido)== null){
				throw new DomainException("c�digo de pedido n�o existe");
			}
	
		}
		if(opcaoConsulta == 2) {
			System.out.println("Informe a data do seu pedido no formato dd-mm-yyyy");
		    String stringDataConsulta = this.entradaPedido.nextLine();
		    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		    LocalDate dataConsulta = LocalDate.parse(stringDataConsulta,formatter);
		    pedidoRepositorio.buscarPedidoData(dataConsulta);
		    
		}
	}


	private void realizarPedido(Pedido pedidoDesejado, String retorno) {
		while(retorno.equalsIgnoreCase("S")) {
			
			ItemPedido itemPedidoDesejado = new ItemPedido();
			System.out.print("Informe o c�digo do produto que voc� deseja comprar: ");
			Integer codigoProduto = Integer.parseInt(entradaPedido.nextLine());
			
			if(produtoRepositorio.buscarProdutoPorCodigo(codigoProduto) == null) {
				throw new DomainException("c�digo de produto informado n�o existe");
			}
			
			Produto produtoDesejado = produtoRepositorio.buscarProdutoPorCodigo(codigoProduto);
			itemPedidoDesejado.setPreco(produtoDesejado.getValor());

			System.out.print("Informe a quantidade do produto que deseja comprar: ");
			int qtdDesejada = Integer.parseInt(this.entradaPedido.nextLine());
			
			if(qtdDesejada > produtoDesejado.getEstoque()) {
				throw new DomainException("quantidade informada � superior ao estoque dispon�vel");
			}
			
			itemPedidoDesejado.setQuantidade(qtdDesejada);
			
			itemPedidoDesejado.setProduto(produtoDesejado);
			pedidoDesejado.addItemPedido(itemPedidoDesejado);
			serviceEstoque.decrementarEstoque(itemPedidoDesejado.getProduto(), itemPedidoDesejado.getQuantidade());
			
			System.out.print("Deseja continuar comprando? S/N: ");
			retorno = entradaPedido.nextLine();                  		
		}
	}


	private void mostrarProdutoEstoque(List<Produto> listaProduto) {
        
        for(Produto produto : listaProduto) {
            System.out.println("C�digo: " + produto.getCodigo()
            					+ " | Nome: " + produto.getNome()
            					+ " | Preco: " + produto.getValor()
                                + " | Quantidade em Estoque: " + produto.getEstoque());
        }
    }
}
