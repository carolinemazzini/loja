package com.peixoto.loja.domain;

import java.util.Objects;

public class ItemPedido {
	
    private Produto produto;
    private Integer quantidade;
    private Double preco;
    
    public ItemPedido() {	
	}
    
	public ItemPedido(Produto produto, Integer quantidade) {
		this.produto = produto;
		this.quantidade = quantidade;
	}
  
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public Double getPreco() {
		return preco;
	}	

	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(produto, quantidade);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedido other = (ItemPedido) obj;
		return Objects.equals(produto, other.produto) && Objects.equals(quantidade, other.quantidade);
	}
	
	@Override
	public String toString() {
		return  " Codigo: " + produto.getCodigo() +", Produto: " + produto.getNome() + ", quantidade: " + quantidade + ", pre�o: R$" + preco;
	}
 
	
}