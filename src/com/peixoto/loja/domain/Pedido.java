package com.peixoto.loja.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Pedido {
    private Integer codigo;
    private List<ItemPedido> listaItemPedido;
    private LocalDate dataPedido;
    private Cliente cliente;
    
    public Pedido() {
		listaItemPedido = new ArrayList<ItemPedido>();
	}
    
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer integer) {
		this.codigo = integer;
	}
	public List<ItemPedido> getListaItemPedido() {
		return listaItemPedido;
	}
	
	public void setListaItemPedido(List<ItemPedido> listaItemPedido) {
		this.listaItemPedido = listaItemPedido;
	}

	public LocalDate getDataPedido() {
		return dataPedido;
	}
	public void setDataPedido(LocalDate dataPedido) {
		this.dataPedido = dataPedido;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void addItemPedido (ItemPedido itemPedido) {
		 listaItemPedido.add(itemPedido);
	    }
	 
	 public void removeItemPedido (ItemPedido itemPedido) {
		 listaItemPedido.remove(itemPedido);
	 }
	 
	 public void alteraItemPedido (ItemPedido itemPedido) {
		 listaItemPedido.set(0, itemPedido);
	 }
	  
	public String mostrarPedido () {
			return "Os seu pedido atual �: "
					+ listaItemPedido
					+ "\n";
	    }
	
    public Double valorTotalItemPedido (ItemPedido itemPedido) {
    	Double valorTotal = itemPedido.getQuantidade() * itemPedido.getPreco();
        return valorTotal;
    } 
    
    public Double valorTotalPedido () {
    	Double soma = 0.0;
    	for(ItemPedido itemPedidoNaLista : getListaItemPedido()) {
    		soma = soma + valorTotalItemPedido(itemPedidoNaLista);
    	}
    	return soma;
    }

	@Override
	public String toString() {
		return "O seu pedido �: "
				+ "\n" 
				+ "Data: " + dataPedido.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
				+ "\n" 
				+ listaItemPedido
				+ "\n";
	}

}