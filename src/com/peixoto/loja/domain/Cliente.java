package com.peixoto.loja.domain;

import java.time.LocalDate;

public class Cliente {
	
    private String cpf;
    private String nome;
    private Endereco endereco;
    private LocalDate dataReativacao;
    
	public Cliente() {
	}
    
    public Cliente(String cpf, String nome, Endereco endereco) {
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endereco;
	}
	

	public Cliente(String cpf, String nome, Endereco endereco, LocalDate dataReativacao) {
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endereco;
		this.dataReativacao = dataReativacao;
	}
    
    

	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public LocalDate getDataReativacao() {
		return dataReativacao;
	}
	public void setDataReativacao(LocalDate dataReativacao) {
		this.dataReativacao = dataReativacao;

	}
	
	 public LocalDate calcularDataReativacao () {
	    	LocalDate dataReativacao = LocalDate.now().minusDays(10).plusDays(this.getNome().length());
	    	return dataReativacao;
	    }
	    
	 public String verificarDataReativacao () {
	    	if(calcularDataReativacao().isAfter(LocalDate.now())){
	    		return " expirada: " + calcularDataReativacao();
	    	}
	    	return this.getDataReativacao().toString();
	    }
	 
	 
}
