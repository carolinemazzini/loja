package com.peixoto.loja.repositorio;

import java.util.ArrayList;
import java.util.List;

import com.peixoto.loja.domain.Produto;

public class ProdutoRepositorio implements Repository <Produto>{
	

    private static List<Produto> listaProdutos = new ArrayList<Produto>();
    
	   public ProdutoRepositorio() {
	        if(listaProdutos.isEmpty()) {
	            listaProdutos.add(new Produto(1, "havaianas", "havaianas branca", 12.5, 100));
	            listaProdutos.add(new Produto(2, "colgate", "colgate MPA", 1.5, 200));
	            listaProdutos.add(new Produto(3, "havaianas", "havaianas preta", 15.60, 300));
	            listaProdutos.add(new Produto(4, "havaianas", "havaianas rosa", 15.60, 0));
	        }
	    }
    
    public void salvar(Produto produto) {
        listaProdutos.add(produto);
    }

    public List<Produto> buscarTodos() {
        return listaProdutos;
    }
    
    public List<Produto> buscarProdutosComEstoque() {
    	List<Produto> listaProdutosComEstoque = new ArrayList<Produto>();
    	Produto produtoEncontrado = null;
    	for(Produto produtoComEstoque : listaProdutos) {
            if(produtoComEstoque.getEstoque() > 0) {
               produtoEncontrado = produtoComEstoque;
               listaProdutosComEstoque.add(produtoEncontrado);
                }
            }
    	return listaProdutosComEstoque;
    }
    
    public Produto buscarProdutoPorCodigo(Integer codigo) {
        Produto produtoEncontrado = null;
        for(Produto produtoNaLista : listaProdutos) {
            if(produtoNaLista.getCodigo().equals(codigo)) {
                produtoEncontrado = produtoNaLista;
            }
        }
        return produtoEncontrado;
    }
    
    public Integer criarCodigoProduto(Produto produto) {
  	   return listaProdutos.size() + 1;
     }
      
}