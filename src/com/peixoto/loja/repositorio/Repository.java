package com.peixoto.loja.repositorio;

import java.util.List;


public interface Repository<T> {
	
	
	public void salvar(T object);
	
	public List <T> buscarTodos();
	
}
