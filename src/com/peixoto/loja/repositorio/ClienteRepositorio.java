package com.peixoto.loja.repositorio;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.peixoto.loja.domain.Cliente;
import com.peixoto.loja.domain.Endereco;

public class ClienteRepositorio implements Repository <Cliente> {
	
	private static List<Cliente> listaClientes = new ArrayList<Cliente>();
	
	public ClienteRepositorio() {
	        if(listaClientes.isEmpty()) {	        	
	        	LocalDate data = LocalDate.now();
	         	Endereco enderecoSalvo = new Endereco("Rua Senador","170", "Santa Monica", "Taquaritinga", "15900");
	        	listaClientes.add(new Cliente("38867285874", "Carol", enderecoSalvo, data));
	        }
	    }
	
    public void salvar(Cliente cliente) {
        listaClientes.add(cliente);
    }

    public List<Cliente> buscarTodos() {
        return listaClientes;
    }

    public Cliente buscarClientePorCPF(String cpf) {
        Cliente clienteEncontrado = null;
        for(Cliente clienteNaLista : listaClientes) {
            if(clienteNaLista.getCpf().equals(cpf)) {
                clienteEncontrado = clienteNaLista;
            }
        }
        return clienteEncontrado;
    }
    
    public List <Cliente> buscarClientePorNome(String nome) {
        List<Cliente> listaClienteEncontrado = new ArrayList<Cliente>();
        
        for(Cliente clienteNaLista : listaClientes) {
            if(clienteNaLista.getNome().trim().toLowerCase().contains(nome.trim().toLowerCase())) {
                listaClienteEncontrado.add(clienteNaLista);
            }
        }
        return listaClienteEncontrado;
    }

}