package com.peixoto.loja.repositorio;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.peixoto.loja.domain.Pedido;

public class PedidoRepositorio implements Repository <Pedido>{
	
	final private List<Pedido> listaPedido = new ArrayList<Pedido>();

    public void salvar(Pedido pedido) {
    	
    	pedido.setCodigo(criarCodigoPedido(pedido));
    	pedido.setDataPedido(LocalDate.now());
        listaPedido.add(pedido);
    }

	public List<Pedido> buscarTodos() {
        return listaPedido;
    }

    public Pedido buscarPedidoCodigo(Integer codigo) {
        Pedido pedidoEncontrado = null;
        for(Pedido pedidoNaLista : listaPedido) {
            if(pedidoNaLista.getCodigo().equals(codigo)) {
                pedidoEncontrado = pedidoNaLista;
            }
        }
        return pedidoEncontrado;
    }
    
    public void buscarPedidoData(LocalDate dataPedido) {
        Pedido pedidoEncontrado = null;
        for(Pedido pedidoNaLista : listaPedido) {
            if(pedidoNaLista.getDataPedido().equals(dataPedido)) {
                pedidoEncontrado = pedidoNaLista;
                System.out.println(pedidoEncontrado);
            }
        }
        
    }
     
   public Integer criarCodigoPedido(Pedido pedido) {
	   return listaPedido.size() + 1;
   }
    
}